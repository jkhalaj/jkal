(function( $ ) {
    $.fn.jkal = function(options) {
        var d = new Date();
        
        var opts = $.extend( {
            year : '',
            month : '',
            day : '',
        }, options);
        
        j = 0;    
        return this.each(function() {  
            var containingElement = this;
            var year = opts.year;
            var month = parseInt(opts.month);
            var day = parseInt(opts.day);
            
            $(containingElement).after('<div class="jkal-container-' + j + '"><div class="jkal-year"></div><div class="jkal-month"><span class="jkal-single-month jkal-jan">Jan</span><span class="jkal-single-month jkal-feb">Feb</span><span class="jkal-single-month jkal-mar">Mar</span><span class="jkal-single-month jkal-apr">Apr</span><span class="jkal-single-month jkal-may">May</span><span class="jkal-single-month jkal-jun">Jun</span><span class="jkal-single-month jkal-jul">Jul</span><span class="jkal-single-month jkal-aug">Aug</span><span class="jkal-single-month jkal-sep">Sep</span><span class="jkal-single-month jkal-oct">Oct</span><span class="jkal-single-month jkal-nov">Nov</span><span class="jkal-single-month jkal-dec">Dec</span></div><div class="jkal-day"></div></div>');
    	    j++;
    	    
    	    var nextDiv = $(containingElement).next();
            var containingClass = '.' + nextDiv.attr('class');
    	    
    	    height = $(containingElement).outerHeight();
    	    elo = $(containingElement).offset();
            $(containingClass).css({'left': elo.left, 'top': (elo.top + height)});
            
            // make sure calendar follows the element when window is being resized
            $(window).resize(function() {
			    height = $(containingElement).outerHeight();
                elo = $(containingElement).offset();
                $(containingClass).css({'left': elo.left, 'top': (elo.top + height)});
			});
			
            optsSet = 'yes';
            if (year == '' || month == '' || day == '' ) {
                year = d.getFullYear();
                month = d.getMonth() + 1;
                day = d.getUTCDate();
                optsSet = 'no';
            }
            
            var years = yearRange(year);
            years = yearHTMLFormat(years);
            $(containingClass + ' .jkal-year').html(years);
            $(containingClass + ' .jkal-year').css('left', -570);
            
            var monthNames = ["jkal-jan", "jkal-feb", "jkal-mar", "jkal-apr", "jkal-may", "jkal-jun", "jkal-jul", "jkal-aug", "jkal-sep", "jkal-oct", "jkal-nov", "jkal-dec"];
            
            var monthClass = monthNames[month - 1];
            $(containingClass + ' .' + monthClass).addClass('jkal-active-month'); 
            
            daysInMonth();
            // if opts have been set, show active day
            if (optsSet == 'yes') {
                $(containingClass + ' .jkal-day-' + day).addClass('jkal-active-day');
            }
                        
            function yearRange(year) {
                 var yearArray = new Array();
                 var k = 0
                 for (var i = 8; i >= -8; i--) {
                     yearArray[k] = year - i;
                     k++;
                 }        
                 return yearArray;
            }
            
            function yearHTMLFormat(yearArray) {
                var yearFormatted = new Array();
                var i = 0;
                yearArray.forEach(function(name){
                    if (name == year) {
                        yearFormatted[i] = '<span class="jkal-single-year jkal-active-year">' + name + '</span>';
                    } else {
                        yearFormatted[i] = '<span class="jkal-single-year">' + name + '</span>';
                    }
                    
                    i++;
                });
                return yearFormatted;
            }
            
            function dayHTMLFormat(days) {
                var dayFormatted = new Array();
                for (var i = 1; i <= days; i++) {
                    dayFormatted[i] = '<span class="jkal-single-day jkal-day-'+ i + '">' + i + '</span>';
                }
                $(containingClass + ' .jkal-day').html(dayFormatted);
            }
            
            function daysInMonth() {
                month = $(containingClass + ' .jkal-active-month').index() + 1;
                year = $(containingClass + ' .jkal-active-year').text();
                days = new Date(year, month, 0).getDate();
                dayHTMLFormat(days);
            }
            
            $(containingClass + ' .jkal-year').on('click', '.jkal-active-year', function() {
                return false;
            });
            
            $(containingClass + ' .jkal-year').on('click', '.jkal-single-year:not(.jkal-active-year)', function() {
                if ($(containingClass + ' .jkal-year').is(':animated')) { 
                    return false; 
                } 
                
                $(containingClass + ' .jkal-single-year').each(function(){
                     $(this).removeClass('jkal-active-year');
                })
                $(this).addClass('jkal-active-year');
                var position = $(this).position();
                var positionLeft = parseInt(position.left);
                daysInMonth();
                
                var yearContainerPosition = $(containingClass + ' .jkal-year').position();
                
                callback = function() {
                    var year = $(containingClass + ' .jkal-active-year').text();
                    years = yearRange(year);
                    years = yearHTMLFormat(years);
                    $(containingClass + ' .jkal-year').html(years);
                    
                    if (positionLeft == 570) {
                        $(containingClass + ' .jkal-year').css('left', -positionLeft)
                    } else if (positionLeft == 665) {
                        $(containingClass + ' .jkal-year').css('left', -positionLeft + 95)
                    } else if (positionLeft == 855) {
                        $(containingClass + ' .jkal-year').css('left', -positionLeft + 190)
                    } else if (positionLeft == 950) {
                        $(containingClass + ' .jkal-year').css('left', -positionLeft + 285)
                    }
                };
                
                if (positionLeft == 570) {
                    $(containingClass + ' .jkal-year').animate({
                       left: '+=190px'
                    }, {duration : 450, complete: callback});
                } else if (positionLeft == 665 && parseInt(yearContainerPosition.left) == -665) {
                    $(containingClass + ' .jkal-year').animate({
                       left: '+=190px'
                    }, {duration : 450, complete: callback});
                } else if (positionLeft == 665) {
                    $(containingClass + ' .jkal-year').animate({
                       left: '+=95px'
                    }, {duration : 450, complete: callback});
                } else if (positionLeft == 855 && parseInt(yearContainerPosition.left) == -665) {
                    $(containingClass + ' .jkal-year').animate({
                       left: '-=95px'
                    }, {duration : 450, complete: callback});
                } else if (positionLeft == 855 || positionLeft == 950) {
                    $(containingClass + ' .jkal-year').animate({
                       left: '-=190px'
                    }, {duration : 450, complete: callback});
                } else {
                    $(containingClass + ' .jkal-year').animate({
                        left: '-=190px'
                    }, {duration : 450, complete: callback});
                }
                return false;
            });
            
            $(containingClass + ' .jkal-single-month').on('click', function() {
                $(containingClass + ' .jkal-single-month').each(function(){
                     $(this).removeClass('jkal-active-month');
                })
                $(this).addClass('jkal-active-month');
                daysInMonth();
                return false;
            });
            
            $(containingClass + ' .jkal-day').on('click', '.jkal-single-day', function() {
                var activeYear = $(containingClass + ' .jkal-active-year').text();
                var activeMonth = $(containingClass + ' .jkal-active-month').index() + 1;
                var activeDay = $(this).text();
                var tagName = $(containingElement).prop("tagName");
                
                if (tagName.toLowerCase() == 'input') {
                    $(containingElement).val(activeYear+'-'+pad(activeMonth,2)+'-'+pad(activeDay,2));
                } else {
                    $(containingElement).text(activeYear+'-'+pad(activeMonth,2)+'-'+pad(activeDay,2));
                }
                $(containingClass).fadeOut('fast');
            });
            
            $('html').on('click', function(){
                $(containingClass).fadeOut('fast');
            });
            
            var mouseDown;
            var currentTarget;
            $(containingElement).on({
                "mousedown mouseup": function (e) {
                    mouseDown = e.type === "mousedown";
                    currentTarget = e.target;
                },
                "focus click": function (e) {
                    if (mouseDown && currentTarget === e.target) return;
                    $("[class^=jkal-container-]").each(function(){
                    $(this).fadeOut('fast');   
                });
                    $(containingClass).fadeIn('fast'); 
                    return false;
                }     
            });
            
            function pad (str, max) {
                str = String(str)
                return str.length < max ? pad("0" + str, max) : str;
            }
                       
        }); // END this.each
    };
})( jQuery );